@extends('layouts.app')

@section('content')


<div class="container">
	<h1>My todo</h1>

<table class="table table-hover table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Title</th>
      <th scope="col">Priority</th>
      <th scope="col">Status</th>
      <th scope="col">Delet</th>


    </tr>
  </thead>
  <tbody>
   
  @foreach($todo as $mytodo)
    <tr>
      <th scope="row">{{ $mytodo->id }}</th>
      <td>{{ $mytodo->title }}</td>
      <td>{{ $mytodo->priority }}</td>
      <td> <a href="/todo/switch/{{ $mytodo->id }}" > <button type="button" class="btn btn-primary">{{ $mytodo->status }}</button></a> </td>
      <td> <a href="/todo/switch/{{ $mytodo->id }}" > <button type="button" class="btn btn-danger">Delete</button></a> </td>

    </tr>
    @endforeach

   
    
  </tbody>
</table>
<form method="post" action = "/todo">
	    @csrf

  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationServer01">Title</label>
      <input type="text" class="form-control is-valid" id="validationServer01" name="title" placeholder="title" value="" required>
      <div class="valid-feedback">
        Todo Title
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationServer02">Priority</label>
      <input type="text" class="form-control is-valid" id="validationServer02" name="priority" placeholder="Priority" value="" required>
      <div class="valid-feedback">
        Looks good!
      </div>
    </div>
    
  <button class="btn btn-primary" type="submit" >Add</button>
</form>

</div>
@endsection
