<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Todo;
use App\Http\Resources\Todo as TodoResource;
class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $todo=Todo::all();
        $arr=array('todo'=>$todo);
        return view("todo.create",$arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //



    }

   


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
$todo=new Todo;
//$todo->title=$request->input('title');
$todo->title=$request->input('title');
$todo->priority=$request->input('priority');
$todo->status="open";

$todo->save();
return new TodoResource($todo);

        /*$todo=new Todo;
        $todo->title=$request->input()['title'];
        $todo->priority=$request->input()['priority'];
        $todo->status=1;

        $todo->save();
                return $request->input()['priority'];
*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $todo=Todo::find($id);
        return $todo;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $todo=Todo::find($id);
        $todo->delete();
    }

    public function switch($id){
        $todo=Todo::find($id);
        $status=$todo->status;
        if($status=='open')  {$todo->status = "done"; $todo->save();}
        else if($status=='done')  {$todo->status = "open";$todo->save();}

return redirect()->action(
    'TodoController@index');
    }

    public function ngTodo(){

        $todo=Todo::all();
        return $todo;
    }
}
